<?php
function get_require_path(string $section,string $module="",string $file="",bool $allow_downgrade=true) : ?string
{
    $test_path = "view/".$section."";
    if($module != "") $test_path .= "/".$module;
    if($file != "") $test_path .= "/".$file;
    else $test_path .= "/loader.php";
    if(file_exists($test_path) == true) return $test_path;
    else
    {
        if($allow_downgrade == true)
        {
            if($section == "backend") return get_require_path("public",$module,$file);
            else if($section == "public") return get_require_path("shared",$module,$file);
            else return null;
        }
        else return null;
    }
}
function load_template_file($selected_layout="",$layer="",$allow_downgrade=true,$return_output=false)
{
    global $site_theme;
    $template_file = "theme/".$site_theme."/".$layer."/".$selected_layout.".layout";
    if(file_exists($template_file))
    {
        if($return_output == false) require_once($template_file);
        else return file_get_contents($template_file);
    }
    else
    {
        if($allow_downgrade == true)
        {
            if($layer != "layout") return load_template_file($selected_layout,"layout",false,$return_output);
            else
            {
                echo "Unable to find template file ".$selected_layout."<Br/>";
                if($return_output == true) return "";
            }
        }
        else
        {
            echo "Unable to find template file ".$selected_layout." no downgrade allowed<Br/>";
            if($return_output == true) return "";
        }
    }
}

function is_checked(bool $input_value) : string
{
    if($input_value == true) return " checked ";
    else return "";
}
function load_template($selected_layout="",$allow_downgrade=true)
{
    global $section;
    load_template_file($selected_layout,$section,$allow_downgrade);
}
function redirect($to="",$off_site=false)
{
	if($off_site == true)
	{
		if (!headers_sent()) { header("Location: ".$to.""); }
        else echo "<meta http-equiv=\"refresh\" content=\"0; url=".$to."\">";
	}
	else
	{
		global $template_parts;
        if($template_parts["url_base"] == "") $template_parts["url_base"] = "https://localhost";
		if (!headers_sent()) { header("Location: ".$template_parts["url_base"]."".$to.""); }
        else echo "<meta http-equiv=\"refresh\" content=\"0; url=".$template_parts["url_base"]."/".$to."\">";
	}
}
function clean_and_short_excerpt(string $input,array $allowed_tags=array(),int $max_length)
{
    return clean_excerpt(flame_strip_tags($input,$allowed_tags),$max_length);
}
function clean_excerpt(string $input,int $max_length=200)
{
    if(strlen($input) > $max_length)
    {
        $bits = explode(" ",$input);
        $index = 0;
        $entrys = count($bits);
        $last = "";
        $new_test = "";
        while((strlen($new_test) <= $max_length) && ($index < $entrys))
        {
            $last = $new_test;
            $new_test .= " ".$bits[$index];
            $index++;
        }
        if(strlen($new_test) <= $max_length)
        {
            $last = $new_test;
        }
        return $last;
    }
    else return $input;
}
function flame_strip_tags($html, $allowed_tags=array()) {
  $allowed_tags=array_map("strtolower",$allowed_tags);
  $rhtml=preg_replace_callback('/<\/?([^>\s]+)[^>]*>/i', function ($matches) use (&$allowed_tags) {
    return in_array(strtolower($matches[1]),$allowed_tags)?$matches[0]:'';
  },$html);
  return $rhtml;
}
?>
