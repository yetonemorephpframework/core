<?php
// Please do not edit any values below this line
// ===========================================================================================
$section = "public";
// ^ main switch

$module = "homepage";
$area = "";
$optional = "";
$page = 0;
$load_id = 0;


// ^ uri values

$jquery_addon_script = "";
$debug_text = "";
$template_parts = array(
    "url_base" => "", // set this in config/load
    "html_title" => "",
    "html_title_after" => "", // set this in config/load
    "html_cs_top" => "",
    "page_content" => "",
    "html_js_bottom" => "<script>var url_base = '[[url_base]]';</script> ",
    "html_js_onready" => " ",
);

$page_buffer = "";
?>
