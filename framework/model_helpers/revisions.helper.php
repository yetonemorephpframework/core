<?php
class revisions_helper
{
    public function get_number_of_revisions(string $target_table,int $main_link_id)
    {
        global $sql;
        $where_fields = array(
            array("revision_of_link" => "=")
        );
        $where_values = array(
            array($main_link_id => "i")
        );
        $sql_count = $sql->basic_count($target_table,$where_fields,$where_values);
        if($sql_count["status"] == true) return $sql_count["count"];
        else return 0;
    }
    public function get_list_of_revisions(string $target_table) : array
    {
        global $sql;
        $fields = array();
        $where_config = array(
            "fields"   => array("revision_of_link"),
            "matches"  => array("IS NOT"),
            "values"   => array(null),
            "types"     => array("i")
        );
        $basic_config = array(
            "table" => $target_table,
            "fields" => array("DISTINCT(revision_of_link) as id")
        );
        $sql_reply = $sql->selectV2($basic_config,null,$where_config);
        if($sql_reply["status"] == true)
        {
            $ids = array();
            foreach($sql_reply["dataSet"] as $entry) $ids[] = $entry["id"];
            return array("status"=>true,"message"=>"ok","ids"=>$ids);
        }
        else
        {
            return array("status"=>false,"message"=>$sql_reply["message"]);
        }

    }
}
?>
