Requires

https://bitbucket.org/yetonemorephpframework/db_objects/src/master/   => framework/db_objects

https://bitbucket.org/yetonemorephpframework/mysqli/src/master/   => framework/mysqli

Demo objects require a local database to be created with demo.sql

config can be found in config/db.php
