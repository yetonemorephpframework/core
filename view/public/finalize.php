<?php
$buffer = ob_get_contents();
ob_clean();
$loop = 0;
while($loop < 2)
{
    foreach($template_parts as $key => $value)
    {
        $buffer = str_replace("[[".$key."]]",$value,$buffer);
    }
    $buffer = str_replace("[[MODULE]]",$page,$buffer);
    $buffer = str_replace("[[AREA]]",$optional,$buffer);
    $loop++;
}
echo $buffer;
?>
