<?php
$found_path = get_require_path($section,$module);
if($found_path != null)
{
    // pre main content
    $load_view_files = array("header.php");
    foreach($load_view_files as $loadfile)
    {
        $file_path = get_require_path($section,"",$loadfile);
        if($file_path != null) require_once($file_path);
        if(file_exists("view/".$section."/".$loadfile.".php") == true) require_once("view/".$section."/".$loadfile.".php");
        else if(file_exists("view/public/".$loadfile.".php") == true) require_once("view/public/".$loadfile.".php");
    }
    // display the site
    require_once($found_path);

    // after main content
    $load_view_files = array("footer.php","finalize.php");
    foreach($load_view_files as $loadfile)
    {
        $file_path = get_require_path($section,"",$loadfile);
        if($file_path != null) require_once($file_path);
        if(file_exists("view/".$section."/".$loadfile.".php") == true) require_once("view/".$section."/".$loadfile.".php");
        else if(file_exists("view/public/".$loadfile.".php") == true) require_once("view/public/".$loadfile.".php");
    }
}
else
{
    die("Unsupported [maybe redirect to a 404 page here]");
}
?>
