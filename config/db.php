<?php
class db extends error_logging
{
    protected $dbHost = "localhost";
    protected $dbName = "demo";
    protected $dbUser = "demo";
    protected $dbPass = "demo";
    // ^ default local dev config (please use a connection string)
    // try to avoid using passwords in files ^+^
    function __construct()
    {
        foreach ($_SERVER as $key => $value)
        {
            if (strpos($key, "MYSQLCONNSTR_") === 0)
            {
                // found connection string switch to that
                $this->dbHost = preg_replace("/^.*Data Source=(.+?);.*$/", "\\1", $value);
                $this->dbName = preg_replace("/^.*Database=(.+?);.*$/", "\\1", $value);
                $this->dbUser = preg_replace("/^.*User Id=(.+?);.*$/", "\\1", $value);
                $this->dbPass = preg_replace("/^.*Password=(.+?)$/", "\\1", $value);
                break;
            }
        }
    }
}
?>
