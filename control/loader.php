<?php
include("framework/loader.php");
$redirect = "";
$status = true;
$soft_fail = false;
$reply = array();
if($section != "backend")
{
    if(file_exists("control/public/".$module."/".$area.".php") == true)
    {
        include("control/public/".$module."/".$area.".php");
    }
    else
    {
        $status = false;
        echo "Unknown module/area selected please check and try again";
    }
}
else
{
    if(file_exists("control/backend/".$module."/".$area.".php") == true)
    {
        include("control/backend/".$module."/".$area.".php");
    }
    else
    {
        $status = false;
        echo "Unknown module/area selected please check and try again";
    }
}
if($status == false)
{
    if($soft_fail == false)
    {
        $sql->flagError();
    }
}
$buffer = ob_get_contents();
ob_clean();
$reply["status"] = $status;
$reply["message"] = $buffer;
if($redirect != "")
{
    $reply["redirect"] = "".$template_parts["url_base"]."".$redirect."";
}
$input = new inputFilter();
$no_js_success = $input->postFilter("no_js_redirect_success");
$no_js_failed = $input->postFilter("no_js_redirect_failed");
if($no_js_success != null)
{
    if($status == true) redirect($no_js_success);
    else redirect($no_js_failed);
}
else echo json_encode($reply);
?>
